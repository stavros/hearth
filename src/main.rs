#[macro_use] extern crate clap;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate log;
extern crate directories;
extern crate notify;
extern crate reqwest;
extern crate serde;
#[macro_use] extern crate rouille;
extern crate simple_logger;

mod views;

use directories::{BaseDirs, ProjectDirs};
use log::Level;
use notify::{RecommendedWatcher, RecursiveMode, Watcher};
use std::collections::HashMap;
use std::ffi::OsStr;
use std::fs;
use std::io;
use std::net::{IpAddr, Ipv4Addr, SocketAddr, TcpStream};
use std::process::{self, Command};
use std::sync::mpsc::channel;
use std::sync::Mutex;
use std::thread;
use std::time::{Duration, Instant, SystemTime};
use views::start_api;

// The time of the last filesystem event in the Hearth folder, or None if the files were already
// added to IPFS.
lazy_static! {
    static ref LAST_EVENT_TIME: Mutex<Option<Instant>> = Mutex::new(None);
}

lazy_static! {
    static ref ETERNUM_KEY: Mutex<String> = Mutex::new(String::from(""));
}

lazy_static! {
    static ref IPFS_PID: Mutex<u32> = Mutex::new(0);
}

lazy_static! {
    static ref IPFS_HASHES: Mutex<HashMap<String, String>> = Mutex::new(HashMap::new());
}

lazy_static! {
    static ref HEARTH_DIR: std::path::PathBuf = {
        let base_dirs = BaseDirs::new()
            .expect("Could not get home directory.");
        base_dirs.home_dir().join("Hearth")
    };
}

lazy_static! {
    static ref IPFS_PATH: std::path::PathBuf = {
        let proj_dirs = ProjectDirs::from("io", "eternum",  "hearth")
            .expect("Could not retrieve config directory path.");
        let ipfs_dir = proj_dirs.config_dir().join("ipfs");
        fs::create_dir_all(ipfs_dir.as_path())
            .expect("Could not create IPFS config directory.");
        ipfs_dir
    };
}

lazy_static! {
    static ref IPFS_API_HOST: SocketAddr =
        SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 5001);
}

static IPFS_EXECUTABLE: &str = "ipfs";
static HTTP_INTERFACE: &str = "127.0.0.1:26484";

/// Send a SIGTERM to the IPFS daemon and terminate Hearth one second later.
fn kill_ipfs_and_exit(exit_code: i32) {
    thread::spawn(move || {
        thread::sleep(Duration::from_secs(1));
        process::exit(exit_code);
    });

    let ipfs_pid = { IPFS_PID.lock().unwrap() };
    process::Command::new("kill")
        .args(&[OsStr::new(&*ipfs_pid.to_string())])
        .output()
        .expect("Could not kill IPFS daemon.");
}

/// Add some files to the IPFS daemon and the hashmap.
fn add_files_to_ipfs() {
    let output = Command::new(IPFS_EXECUTABLE)
        .env("IPFS_PATH", &*IPFS_PATH)
        .args(&["add", "-r", HEARTH_DIR.to_str().unwrap()])
        .output()
        .expect("Could not add directory.");
    let ipfs_out = String::from_utf8(output.stdout).unwrap();

    let mut hashes = IPFS_HASHES.lock().unwrap();
    hashes.clear();

    // Parse the IPFS output and add it to the hashmap.
    hashes.extend(ipfs_out.lines().map(|l| {
        let mut words = l.splitn(3, " ");
        let _mode = words.next().unwrap();
        let hash = words.next().unwrap();
        let path = words.next().unwrap();
        let clean_path = path.splitn(2, "/").nth(1).unwrap_or(""); // Remove the "Hearth" component from paths.

        (clean_path.to_string(), hash.to_string())
    }));
}

/// Send the latest root hash to Eternum.
fn notify_eternum() {
    let hash = {
        let hashes = IPFS_HASHES.lock().unwrap();
        hashes.get("").unwrap_or(&String::from("")).clone()
    };

    let eternum_key = {
        let eternum_key = ETERNUM_KEY.lock().unwrap();
        eternum_key.clone()
    };

    if hash == "" || eternum_key == "" {
        debug!(
            "No key ({}) or hash ({}) found, will not notify Eternum...",
            eternum_key, hash
        );
        return;
    }

    debug!("Notifying Eternum with hash {}...", hash);

    let payload: HashMap<&str, _> = [("site_hash", hash)].iter().cloned().collect();

    let eternum_url = format!("https://www.eternum.io/api/site/?key={}", eternum_key);
    let client = reqwest::Client::new();
    let _ = client.put(eternum_url.as_str()).json(&payload).send();

    info!("Successfully notified Eternum.");
}

/// Receive a filesystem event and act on it.
///
/// This does the proper debouncing (on top of the inotify debouncer) to emit the necessary
/// messages to the console when we are expecting changes/are done with changes.
fn notify_ipfs(fs_event: bool) {
    // If this is a filesystem event, just reset the timer.
    let mut last_event_time = LAST_EVENT_TIME.lock().unwrap();

    debug!("Got event, fs_event={:?}", fs_event);

    if last_event_time.is_none() {
        debug!("Last event time is None");
    } else {
        debug!("Last event time is {:?}", last_event_time.unwrap());
    }

    if fs_event {
        if last_event_time.is_none() {
            // This is the first event for now, so emit the log line.
            info!("Change detected in filesystem.");
        }

        // Reset the timestamp to now.
        *last_event_time = Some(Instant::now());
        return;
    } else {
        // This is a timer event, so we should check when the last timer event was.
        if last_event_time.is_none()
            || Instant::now()
                .duration_since(last_event_time.unwrap())
                .as_secs()
                < 5
        {
            // It there was no event, or there hasn't been long enough since the last one, return.
            return;
        }
        info!("Adding files to IPFS...");
        add_files_to_ipfs();
        info!("Finished adding files to IPFS.\n");

        // We added files to IPFS, so we'll set the last_event_time to None.
        *last_event_time = None;

        // Send the updated hash to Eternum.
        thread::spawn(notify_eternum);
    }
}

/// Start the IPFS daemon and wait. If it ever dies, quit the entire program.
fn start_ipfs() {
    info!("Initializing IPFS directory...");

    Command::new(IPFS_EXECUTABLE)
        .env("IPFS_PATH", &*IPFS_PATH)
        .args(&["init"])
        .output()
        .unwrap_or_else(|_| {
            warn!("Could not initialize IPFS.");
            process::exit(1)
        });

    // Set the gateway port.
    Command::new(IPFS_EXECUTABLE)
        .env("IPFS_PATH", &*IPFS_PATH)
        .args(&["config", "Addresses.Gateway", "/ip4/127.0.0.1/tcp/46283"])
        .output()
        .unwrap_or_else(|_| {
            warn!("Could not initialize IPFS.");
            process::exit(1)
        });

    info!("Starting IPFS daemon...");

    // Print the correct message to the console because we'll add some files.
    notify_ipfs(true);

    // Add everything, because files might have been changed while we weren't looking.
    // This is done in a new thread, as the command to run the IPFS daemon farther down will block.
    thread::spawn(|| {
        let timeout = 60;
        info!("Waiting {} seconds for daemon to start up...", timeout);

        let sys_time = SystemTime::now();
        while sys_time
            .duration_since(sys_time)
            .expect("SystemTime::duration_since failed")
            .as_secs()
            < timeout
        {
            let conn = TcpStream::connect_timeout(&IPFS_API_HOST, Duration::from_secs(1));
            if conn.is_ok() {
                info!("Daemon is up, continuing...");
                thread::sleep(Duration::from_secs(1));

                thread::spawn(ping_eternum);

                // Connect this node to Eternum directly.
                Command::new(IPFS_EXECUTABLE)
                    .env("IPFS_PATH", &*IPFS_PATH)
                    .args(&["swarm", "connect", "/dns4/door.eternum.io/tcp/4001/ipfs/QmUAzL9Fpp1fsZnrkANcitMevCoTWKP3TJcgTuc2D3b7vi"]);

                // After all initialization has been done, spawn the cron thread (so we can add
                // files to IPFS at startup as well).
                thread::spawn(cron_thread);
                return;
            }
        }
        // Daemon didn't start up in a minute, kill it and exit.
        warn!(
            "IPFS daemon failed to become responsive in {} seconds, exiting...",
            timeout
        );
        kill_ipfs_and_exit(1);
    });

    let mut child = Command::new(IPFS_EXECUTABLE)
        .env("IPFS_PATH", &*IPFS_PATH)
        .args(&["daemon", "--writable", "--enable-gc", "--migrate"])
        .spawn()
        .unwrap_or_else(|_| {
            warn!("Could not launch IPFS daemon.");
            process::exit(1)
        });

    {
        // Manage the lock here.
        let mut ipfs_pid = IPFS_PID.lock().unwrap();
        *ipfs_pid = child.id();
    }
    child.wait().expect("Failed to wait on IPFS daemon.");

    warn!("IPFS daemon died, exiting...");
    process::exit(1)
}

/// Run notify_ipfs every second, for debouncing.
///
/// notify_ipfs stores the last time an event occurred, and only adds files to IPFS if there's no
/// event after a while. To do that, however, it needs to also run "after a while". This is where
/// this function comes in. It runs notify_ipfs every second, to give it the opportunity to check
/// its timer and add files to IPFS if it needs to.
fn cron_thread() {
    loop {
        thread::sleep(Duration::from_secs(1));
        notify_ipfs(false);
    }
}

/// Handle the inotify events in the Hearth directory.
fn files_changed(op: notify::Op, path: std::path::PathBuf) {
    debug!("Got new event: {:?}, {:?}", op, path);
    if op.intersects(
        notify::op::CREATE
            | notify::op::REMOVE
            | notify::op::RENAME
            | notify::op::WRITE
            | notify::op::RESCAN
            | notify::op::CLOSE_WRITE,
    ) {
        notify_ipfs(true);
    }
}

fn create_watcher() {
    let (tx, rx) = channel();
    let mut watcher: RecommendedWatcher = Watcher::new_raw(tx).unwrap();

    watcher
        .watch(&*HEARTH_DIR, RecursiveMode::Recursive)
        .expect("There was an error when trying to watch the Hearth directory for changes.");

    loop {
        match rx.recv() {
            Ok(notify::RawEvent {
                path: Some(path),
                op: Ok(op),
                cookie: _cookie,
            }) => files_changed(op, path),
            Ok(event) => debug!("Broken event: {:?}", event),
            Err(event) => debug!("Watch error: {:?}", event),
        }
    }
}

/// Send a request to Eternum, for estimating how many Hearth installations there are.
/// IP addresses or other identifying data is not stored.
fn ping_eternum() {
    let output = Command::new(IPFS_EXECUTABLE)
        .env("IPFS_PATH", &*IPFS_PATH)
        .args(&["id", "-f", "<id>"])
        .output()
        .expect("Could not get peer ID.");

    let peer_id = String::from_utf8(output.stdout).unwrap();
    let client = reqwest::Client::new();
    let _ = client
        .get(format!("https://www.eternum.io/track/hearth/?id={}", peer_id).as_str())
        .send();
}

fn main() {
    let matches = clap_app!(Hearth =>
        (author: "by Stavros and Stelios")
        (about: "A personal website publisher")
        (@arg eternum_key: -k --api_key <APIKEY> +takes_value "Your Eternum API key, to automatically update your personal site hash")
        (@arg debug: -d --debug "Print debugging information")
    ).get_matches();

    simple_logger::init_with_level(if matches.is_present("debug") {
        Level::Debug
    } else {
        Level::Info
    })
    .unwrap();

    {
        let mut eternum_key = ETERNUM_KEY.lock().unwrap();
        *eternum_key = String::from(matches.value_of("eternum_key").unwrap_or(""));
    }

    info!("Starting hearth...");

    thread::spawn(start_ipfs);

    info!("Starting API server on {}...", HTTP_INTERFACE);
    thread::spawn(start_api);

    match fs::create_dir(&*HEARTH_DIR) {
        Err(ref e) if e.kind() != io::ErrorKind::AlreadyExists => {
            warn!("Could not create the Hearth directory, for some reason.");
            process::exit(1)
        }
        _ => {}
    }

    create_watcher();
}
