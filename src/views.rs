use rouille::{self, Response};

use serde::Serialize;

use kill_ipfs_and_exit;
use HTTP_INTERFACE;
use IPFS_HASHES;

#[derive(Serialize)]
struct ExitResponse {
    result: String,
}

#[derive(Serialize)]
struct HashResponse {
    result: String,
    hash: String,
    path: String,
}

pub fn start_api() {
    rouille::start_server(HTTP_INTERFACE, move |request| {
        router!(request,
         (POST) (/api/exit/) => {
             kill_ipfs_and_exit(0);
             Response::json(&ExitResponse{result: "ok".to_owned()})
         },
         (GET) (/api/path/) => {
             let path = request.get_param("path").unwrap_or("".to_string());

             let hashes = IPFS_HASHES.lock().unwrap();
             match hashes.get(&path) {
                 Some(hash) => Response::json(&HashResponse{
                     hash: hash.to_string(),
                     path: path.to_string(),
                     result: "ok".to_owned()
                 }),
                 None => Response::empty_404(),
             }
         },
         _ => rouille::Response::empty_404()
        )
    });
}
